-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2017 at 01:21 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `managemen_guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_guru`
--

CREATE TABLE `data_guru` (
  `kode_guru` varchar(50) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `asal` text NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `mapel` varchar(50) NOT NULL,
  `pendidikan_terakhir` varchar(50) NOT NULL,
  `jumlah_jam` int(11) NOT NULL,
  `tanggal_mulai_mengajar` date NOT NULL,
  `username_guru` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_guru`
--

INSERT INTO `data_guru` (`kode_guru`, `nama_guru`, `asal`, `telepon`, `mapel`, `pendidikan_terakhir`, `jumlah_jam`, `tanggal_mulai_mengajar`, `username_guru`, `password`) VALUES
('34', 'qq', 'qq', 'qq', 'qq', 'sd', 90, '0000-00-00', 'qq', 'qq'),
('45', 'jumainah', 'malang', '676', 'ipa', 's2', 78, '2000-10-20', 'tr', 'tr'),
('78', 'wwwwww', 'ww', 'ww', 'wwwwww', '', 0, '0000-00-00', 'ww', 'ww'),
('88', 'ii', '', '', '', 'd3', 0, '2000-10-20', '1', '1'),
('89', 'wwwwww', 'ww', 'ww', 'wwwwww', 'ww', 0, '0000-00-00', 'ww', 'ww'),
('90', 'namaguruharusnya', 'asalharusnya', '0999', 'mapelharusnya', 'smp', 70, '0000-00-00', 'usernameharusnya', 'passwordharusnya'),
('a', 'a', '', '', '', '', 0, '2000-10-20', 'a', 'a'),
('A11', 'andop', 'depok', '1234567890', 'sbk', 's2', 98, '6002-07-19', 'coba', 'coba'),
('aa', 'aa', '', '', '', '', 0, '0000-00-00', 'aa', 'aa'),
('aaa', 'aaaaa', 'aaaaaaaaa', 'a', 'a', 's', 0, '0000-00-00', 's', ''),
('C11', 'Bias Damiasa', 'Malang', '', 'Progweb', 's2', 54, '0200-06-07', 'r4', 'r4'),
('c5', 'Andini Izza Safitri', 'dunia lain', '0341343660', 'science', 'tidak sekolah', 1001, '2000-10-20', 'andin', 'iya'),
('kode', 'nama', 'asal', 'telepon', 'mapel', '', 0, '2000-10-20', 'username', 'password'),
('q2', 'q', 'q', 'q', 'q', 'q', 0, '0000-00-00', 'q', 'e'),
('qwerty', 'erty', '', '', '', 'sma', 0, '0000-00-00', 'qqq', 'qqq');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_guru`
--
ALTER TABLE `data_guru`
  ADD PRIMARY KEY (`kode_guru`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
